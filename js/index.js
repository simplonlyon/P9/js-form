/*
1. Rajouter un évènement au submit sur le formulaire et le neutraliser avec preventDefault() puis faire un console.log pour voir si ça marche
2. Récupérer les valeurs des inputs (et du select) du formulaire et les mettre dans des variables (faire un console.log de ces variables pour voir si ça fonctionne)


Suite :
Faire en sorte qu'au submit du formulaire on rajoute un nouvel article dans la div de gauche qui aura comme valeurs celles du formulaire
1. Commencer juste par faire en sorte de créer une div en javascript (avec le createElement) et de l'append dans la row où il y a les autres articles
2. Une fois que ça marche, rajouter une classe à cette div pour lui mettre les col bootstrap pareil que sur les autres div de cette row
3. Créer ensuite un nouvel élément de type article et l'append dans la div créée précédemment
4. Créer un paragraphe et l'ajouter à l'article créé précédemment et mettre en textContent de ce paragraph le name de la person
5. Faire pareil pour l'age et le langage favoris (un paragraphe avec du textContent)
*/

//On crée un tableau qui "stockera" sous forme de données brut les
//différentes personnes qu'on ajoutera avec le formulaire
let tab = [
    { name: 'test1', surname: 'test1', age: 10, fav: 'HTML' },
    { name: 'test3', surname: 'test3', age: 30, fav: 'HTML' },
];

//On lance une première fois la fonction display en lui donnant le tableau
//en argument pour qu'il calcule l'affichage initial
display(tab);


//On sélectionne l'élément form dans le html
let form = document.querySelector('form');
//On lui ajoute un évènement qui se déclenchera lorsqu'on soumettra le formulaire
form.addEventListener('submit', function (event) {
    //On utilise le preventDefault pour annuler le comportement par 
    //défaut du formulaire : grâce à ça, la page ne se rechargera pas
    event.preventDefault();

    /**
     * On crée un objet personNew qui va servir à représenter la personne
     * créée par le formulaire. On dit qu'une personne c'est quelque chose
     * qui a un name, un age, un surname et un langage favori.
     * On assigne à chacune de ces propriétés la valeur contenue dans 
     * l'input correspondant
     */
    let personNew = {
        name: document.querySelector('#name').value,
        age: document.querySelector('#age').value,
        surname: document.querySelector('#surname').value,
        fav: document.querySelector('#fav').value,
    };
    //On ajoute la personne créée au tableau
    tab.push(personNew);
    //On relance le calcul de l'affichage à chaque fois qu'on change
    //les données (le tableau de personnes)
    display(tab);



});

/**
 * La fonction display sert à générer l'affichage HTML en se basant sur
 * un tableau de données. Elle a vocation à être relancée à chaque fois
 * qu'on modifie les données.
 * @param {person[]} tabPersons Le tableau de personnes qu'on veut afficher
 */
function display(tabPersons) {
    //On sélectionne la div .row à laquelle on a mis un id list
    let list = document.querySelector('#list');
    //On remet l'affichage à zéro avant de le regénérer
    list.innerHTML = '';
    //On fait une boucle sur toutes les personnes du tableau
    for (const person of tabPersons) {
        //Pour chaque personne du tableau...
        //On crée un nouvel élément div qu'on stock dans une variable
        let div = document.createElement('div');
        //on ajoute l'élément div dans cet élément #list
        list.appendChild(div);

        //on ajoute la classe .col-sm-6 à la div
        div.classList.add('col-sm-6');

        //On crée un élément article
        let article = document.createElement('article');
        //On l'ajoute à la div qu'on a créée juste avant
        div.appendChild(article);

        //On crée un élément paragraphe
        let paraName = document.createElement('p');
        //On l'ajoute à l'article créé juste avant
        article.appendChild(paraName);
        //On lui met comme contenu textuel le nom et le prénom de la person
        paraName.textContent = person.name + ' ' + person.surname;

        //On fait pareil pour les autres propriétés de la personne
        let paraAge = document.createElement('p');
        article.appendChild(paraAge);
        paraAge.textContent = person.age;
        let paraFav = document.createElement('p');
        article.appendChild(paraFav);
        paraFav.textContent = person.fav;
    }
}
